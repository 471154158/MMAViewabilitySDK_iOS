//
//  NormalViewController.m
//  Demo
//
//  Created by huangli on 2018/12/7.
//  Copyright © 2018年 Admaster. All rights reserved.
//

#import "NormalViewController.h"
#import "SecondViewController.h"

#import "MobileTracking.h"

@interface NormalViewController () <UIScrollViewDelegate,WKNavigationDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *bottomScrollView;
@property (weak, nonatomic) IBOutlet UIButton *adView;


@property (nonatomic, strong) WKWebView *  htmlWebVIew;

@property (assign, nonatomic)BOOL isPop;

@property (nonatomic, strong)NSString *clickUrl;

@property (nonatomic, assign)DisplayType type;

@end

//普通点击url
NSString *const normalClickUrl = @"https://tyfx.m.cn.miaozhen.com/x/k=2122669&p=7P5QE&dx=__IPDX__&rt=2&ns=__IP__&ni=__IESID__&v=__LOC__&xa=__ADPLATFORM__&tr=__REQUESTID__&mo=__OS__&m0=__OPENUDID__&m0a=__DUID__&m1=__ANDROIDID1__&m1a=__ANDROIDID__&m2=__IMEI__&m4=__AAID__&m5=__IDFA__&m6=__MAC1__&m6a=__MAC__&nd=__DRA__&np=__POS__&nn=__APP__&nc=__VID__&nf=__FLL__&ne=__SLL__&ng=__CTREF__&nx=__TRANSID__&o=";

//普通曝光url
NSString *const normalViewUrl = @"https://tyfx.m.cn.miaozhen.com/x/k=2122669&p=7P5QE&dx=__IPDX__&rt=2&ns=__IP__&ni=__IESID__&v=__LOC__&xa=__ADPLATFORM__&tr=__REQUESTID__&mo=__OS__&m0=__OPENUDID__&m0a=__DUID__&m1=__ANDROIDID1__&m1a=__ANDROIDID__&m2=__IMEI__&m4=__AAID__&m5=__IDFA__&m6=__MAC1__&m6a=__MAC__&nd=__DRA__&np=__POS__&nn=__APP__&nc=__VID__&nf=__FLL__&ne=__SLL__&ng=__CTREF__&nx=__TRANSID__&o=";


//可视化点击url
NSString *const displayClickUrl = @"https://tyfx.m.cn.miaozhen.com/x/k=2128485&p=7P5QE&dx=__IPDX__&rt=2&ns=__IP__&ni=__IESID__&v=__LOC__&xa=__ADPLATFORM__&tr=__REQUESTID__&mo=__OS__&m0=__OPENUDID__&m0a=__DUID__&m1=__ANDROIDID1__&m1a=__ANDROIDID__&m2=__IMEI__&m4=__AAID__&m5=__IDFA__&m6=__MAC1__&m6a=__MAC__&nd=__DRA__&np=__POS__&nn=__APP__&nc=__VID__&nf=__FLL__&ne=__SLL__&ng=__CTREF__&nx=__TRANSID__&o=";

//可视化曝光url
NSString *const displayViewUrl = @"https://tyfx.m.cn.miaozhen.com/x/k=2128485&p=7P5QE&dx=__IPDX__&rt=2&ns=__IP__&ni=__IESID__&v=__LOC__&xa=__ADPLATFORM__&tr=__REQUESTID__&mo=__OS__&m0=__OPENUDID__&m0a=__DUID__&m1=__ANDROIDID1__&m1a=__ANDROIDID__&m2=__IMEI__&m4=__AAID__&m5=__IDFA__&m5a=__IDFV__&m6=__MAC1__&m6a=__MAC__&nd=__DRA__&np=__POS__&nn=__APP__&nc=__VID__&nf=__FLL__&ne=__SLL__&ng=__CTREF__&nx=__TRANSID__&o=";


#define AScreenHeight           ([UIScreen mainScreen].bounds.size.height == 480 ?  (kScreenHeight/667/0.83):(kScreenHeight/667))
#define kScreenHeight [UIScreen mainScreen].bounds.size.height
@implementation NormalViewController

- (instancetype)initWithType:(DisplayType)type
{
    self = [super init];
    if (self) {
        _type = type;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _isPop = NO;
    _bottomScrollView.delegate = self;
    
 
//   _testView.tag = 999;
//    _testView.backgroundColor=nil;
 //  _testView.image = [UIImage imageNamed:@"landingPageIcon"];
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self loadData];

}
- (void)loadData {
    //            广告加载出来后调用
    switch (_type) {
        case Normal:
        {
       
            //    普通曝光
            _adView.hidden = NO;
 
//            [[MobileTracking sharedInstance] jsView:normalViewUrl ad:_adView];
            [[MobileTracking sharedInstance] displayImp:normalViewUrl ad:_adView impressionType:0 succeed:^(NSString *eventType) {
                //监测代码发送成功
         } failed:^(NSString *errorMessage) {
              //监测代码发送失败
         }];
           
            _clickUrl = [NSString stringWithFormat:@"%@", normalClickUrl];
            self.title = @"曝光及点击";
            
        }
            break;
        case Display:
        {
            _adView.hidden = NO;
            
            //            可视化曝光+可视化点击
            //    可视化曝光
            [[MobileTracking sharedInstance] displayImp:normalViewUrl ad:_adView impressionType:1 succeed:^(NSString *eventType) {
                //监测代码发送成功
                NSLog(@"%@",eventType);
         } failed:^(NSString *errorMessage) {
              //监测代码发送失败
             NSLog(@"%@",errorMessage);
         }];
           
           
            
              
         
               
             
            _clickUrl = [NSString stringWithFormat:@"%@", displayClickUrl];
            self.title = @"Display可见曝光";
            
        
        }
            break;
        case HTMLbrt1:
        {
         
            _adView.hidden = YES;
          
            NSString * path_me = @"http://omi-api-test.cn.miaozhen.com/btr-beta/html_img.html";
       
            [self.htmlWebVIew loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:path_me]]];

          self.htmlWebVIew.navigationDelegate = self;
            
            _clickUrl = [NSString stringWithFormat:@"%@", displayClickUrl];
            
            
            self.title = @"HTML曝光 BTR: YES";
            
            
            
            
            
            
        }
            
            break;
        case HTMLbrt0:
        {
            
            _adView.hidden = YES;
            
         
           
            NSString * path_nome =@"http://omi-api-test.cn.miaozhen.com/btr-alpha/html_Img_error.html";
          
            [self.htmlWebVIew loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:path_nome]]];
          
          
           
            self.htmlWebVIew.navigationDelegate = self;
          
       //http://10.203.11.237/btr.html
            //https://omsdk-files.s3-us-west-2.amazonaws.com/creatives/html_display_creative.html
       
             
            _clickUrl = [NSString stringWithFormat:@"%@", displayClickUrl];
            
            
            self.title = @"HTML曝光 BTR: NO";
            
            
            
            
            
            
        }
            
            break;
        case HTMLIMP:
        {
            
            _adView.hidden = YES;
            
         
//            NSString * path_me = @"http://omi-api-test.cn.miaozhen.com/btr-beta/plan-b/html_img.html";
            
           NSString * path_me =@"http://omi-api-test.cn.miaozhen.com/btr-beta/plan-b/html_Img_error.html";
            
           
            
            [self.htmlWebVIew loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:path_me]]];
          
          
           
       self.htmlWebVIew.navigationDelegate = self;
 
       
             
            _clickUrl = [NSString stringWithFormat:@"%@", displayClickUrl];
            
            
            self.title = @"HTML可见曝光(不注入)";
            
            
            
            
            
            
        }
            
            break;
        default:
            break;
    }
}
#pragma mark - webViewDelegate
-(void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation{
    
    switch (_type) {
        case HTMLIMP:
            [[MobileTracking sharedInstance] webViewImp:normalViewUrl ad:_htmlWebVIew  impressionType:1 isInjectJs:NO succeed:^(NSString *eventType) {
                //监测代码发送成功
                NSLog(@"%@",eventType);
         } failed:^(NSString *errorMessage) {
              //监测代码发送失败
             NSLog(@"%@",errorMessage);
         }];
            break;
            
        default:
         
            break;
    }
    
    
}
-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{

    switch (_type) {
//        case HTMLIMP:
//            [[MobileTracking sharedInstance] webViewImp:normalViewUrl ad:_htmlWebVIew  impressionType:1 isInjectJs:YES succeed:^(NSString *eventType) {
//                //监测代码发送成功
//                NSLog(@"%@",eventType);
//         } failed:^(NSString *errorMessage) {
//              //监测代码发送失败
//             NSLog(@"%@",errorMessage);
//         }];
//            break;
            
    case HTMLbrt0:
    case HTMLbrt1:
            [[MobileTracking sharedInstance] webViewImp:normalViewUrl ad:_htmlWebVIew impressionType:0  isInjectJs:YES succeed:^(NSString *eventType) {
                //监测代码发送成功
                NSLog(@"%@",eventType);
         } failed:^(NSString *errorMessage) {
              //监测代码发送失败
             NSLog(@"%@",errorMessage);
         }];
            break;
    }
    
   
}
 
 
- (IBAction)pushViewController:(id)sender {
    
    _isPop = YES;
    SecondViewController *secondVC = [[SecondViewController alloc]init];
    [self.navigationController pushViewController:secondVC animated:YES];
//    点击
    [[MobileTracking sharedInstance]click:_clickUrl succeed:^(NSString *eventType) {
        NSLog(@"%@",eventType);
    } failed:^(NSString *errorMessage) {
         NSLog(@"%@",errorMessage);
    }];
    
}
- (WKWebView *)htmlWebVIew{
    if(_htmlWebVIew == nil){
        
       
        
        _htmlWebVIew = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 300)];
        
        [_bottomScrollView addSubview:_htmlWebVIew];
        
  
        
        
        
    }
    return _htmlWebVIew;
}

 

@end
