//
//  MMA_WebViewScriptMessageDelegate.h
//  MobileTracking
//
//  Created by DeveWang on 2020/11/23.
//  Copyright © 2020 Admaster. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface MMA_WebViewScriptMessageDelegate : NSObject

@property (nonatomic, weak) id<WKScriptMessageHandler> scriptDelegate;

- (instancetype)initWithDelegate:(id<WKScriptMessageHandler>)scriptDelegate;

@end

NS_ASSUME_NONNULL_END
