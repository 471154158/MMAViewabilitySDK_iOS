//
//  MMA_WebViewScriptMessageDelegate.m
//  MobileTracking
//
//  Created by DeveWang on 2020/11/23.
//  Copyright © 2020 Admaster. All rights reserved.
//

#import "MMA_WebViewScriptMessageDelegate.h"

@implementation MMA_WebViewScriptMessageDelegate
- (instancetype)initWithDelegate:(id<WKScriptMessageHandler>)scriptDelegate {
    self = [super init];
    if (self) {
        _scriptDelegate = scriptDelegate;
    }
    return self;
}

#pragma mark - WKScriptMessageHandler
- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
    
    if ([self.scriptDelegate respondsToSelector:@selector(userContentController:didReceiveScriptMessage:)]) {
        [self.scriptDelegate userContentController:userContentController didReceiveScriptMessage:message];
    }
}
@end
