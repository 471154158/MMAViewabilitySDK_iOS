//
//  WKUserContentController+MMA_WKUserContentController.m
//  MobileTracking
//
//  Created by DeveWang on 2020/11/20.
//  Copyright © 2020 Admaster. All rights reserved.
//

#import "WKUserContentController+MMA_WKUserContentController.h"
#import <objc/message.h>
@implementation WKUserContentController (MMA_WKUserContentController)
//- (void)viewWebView:(NSString *)url ad:(WKWebView *)adView isVideo:(BOOL)isVideo impressionType:(NSInteger)type   videoPlayType:(NSInteger)playType  succeed:(void(^)(NSString * eventType))succeedBlock failed:(void(^)(NSString * errorMessage))failedBlock
-(void)setUrl:(NSString *)url{
    @try {
    objc_setAssociatedObject(self, "mma_url", url, 1);
    } @catch (NSException *exception) {
    }
}
-(NSString *)url{
    @try {
    return  objc_getAssociatedObject(self, "mma_url");
    } @catch (NSException *exception) {
    }
}
-(void)setAdView:(WKWebView *)adView{
    @try {
    objc_setAssociatedObject(self, "mma_adView", adView, 1);
    } @catch (NSException *exception) {
    }
}
-(WKWebView *)adView{
    @try {
    return  objc_getAssociatedObject(self, "mma_adView");
    } @catch (NSException *exception) {
    }
}
-(void)setIsVideo:(BOOL )isVideo{
    @try {
    objc_setAssociatedObject(self, "mma_isVideo", [NSNumber numberWithBool:isVideo], 1);
    } @catch (NSException *exception) {
    }
}
-(BOOL )isVideo{
    @try {
    return  [objc_getAssociatedObject(self, "mma_isVideo") boolValue];
    } @catch (NSException *exception) {
    }
}
-(void)setType:(NSInteger )type{
    @try {
    objc_setAssociatedObject(self, "mma_type", [NSNumber numberWithInteger:type], 1);
    } @catch (NSException *exception) {
    }
}
-(NSInteger )type{
    @try {
    return  [objc_getAssociatedObject(self, "mma_type") integerValue];
    } @catch (NSException *exception) {
    }
}
-(void)setPlayType:(NSInteger )playType{
    @try {
    objc_setAssociatedObject(self, "mma_playType", [NSNumber numberWithInteger:playType], 1);
    } @catch (NSException *exception) {
    }
}
-(NSInteger )playType{
    @try {
    return  [objc_getAssociatedObject(self, "mma_playType") integerValue];
    } @catch (NSException *exception) {
    }
}
-(void)setSucceedBlock:(void(^)(id))succeedBlock{
    @try {
    objc_setAssociatedObject(self, "mma_succeedBlock", succeedBlock, 1);
} @catch (NSException *exception) {
}
}
-(void(^)(id))SucceedBlock{
    @try {
    return  objc_getAssociatedObject(self, "mma_succeedBlock");
} @catch (NSException *exception) {
}
}
-(void)setFailedBlock:(void(^)(id))failedBlock{
    @try {
    objc_setAssociatedObject(self, "mma_failedBlock", failedBlock, 1);
} @catch (NSException *exception) {
}
}

-(void(^)(id))failedBlock{
    @try {
    return  objc_getAssociatedObject(self, "mma_failedBlock");
} @catch (NSException *exception) {
}
}
@end
