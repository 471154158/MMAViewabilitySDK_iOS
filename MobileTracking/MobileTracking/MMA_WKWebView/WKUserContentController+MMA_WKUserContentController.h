//
//  WKUserContentController+MMA_WKUserContentController.h
//  MobileTracking
//
//  Created by DeveWang on 2020/11/20.
//  Copyright © 2020 Admaster. All rights reserved.
//

#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WKUserContentController (MMA_WKUserContentController)
@property(nonatomic,strong)NSString * url;
@property(nonatomic,strong)WKWebView *adView;
@property(nonatomic,assign)BOOL isVideo;
@property(nonatomic,assign)NSInteger type;
@property(nonatomic,assign)NSInteger playType;
-(void)setSucceedBlock:(void(^)(id))succeedBlock;
-(void(^)(id))SucceedBlock;
-(void)setFailedBlock:(void(^)(id))failedBlock;
-(void(^)(id))failedBlock;
 
 
@end

NS_ASSUME_NONNULL_END
