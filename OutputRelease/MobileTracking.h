//
//  MobileTracking.h
//  MobileTracking
//
//  Created by Wenqi on 14-3-11.
//  Copyright (c) 2014年 Admaster. All rights reserved.
//

//#define MMA_SDK_VERSION @"V2.2.6"

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface MobileTracking : NSObject

 
+ (MobileTracking *)sharedInstance;

// 配置远程XML配置文件地址
- (void)configFromUrl:(NSString *)url;

// 是否开启调试日志
- (void)enableLog:(BOOL)enableLog;

// 点击
- (void)click:(NSString *)url succeed:(void(^)(NSString * eventType))succeedBlock failed:(void(^)(NSString * errorMessage))failedBlock;

/* display
 url:监测的链接
 adView:监测的广告视图对象
 type: 曝光:0；可见曝光：1
 */
- (void)displayImp:(NSString *)url ad:(UIView *)adView impressionType:(NSInteger)type succeed:(void(^)(NSString * eventType))succeedBlock failed:(void(^)(NSString * errorMessage))failedBlock;
 
/* video
 url:监测的链接
 adView:监测的广告视图对象
 type: 曝光:0；可见曝光：1
 playType: 自动播放:1;手动播放:2;无法识别:0
   
 */
- (void)videoImp:(NSString *)url ad:(UIView *)adView impressionType:(NSInteger)type  videoPlayType:(NSInteger)playType succeed:(void(^)(NSString * eventType))succeedBlock failed:(void(^)(NSString * errorMessage))failedBlock;

/* WebView html display曝光
 url:监测的链接
 adView:监测的WKWebView视图对象
 type: 曝光:0；可见曝光：1
 */
- (void)webViewImp:(NSString *)url ad:(WKWebView *)adView impressionType:(NSInteger)type isInjectJs:(BOOL)isInject succeed:(void(^)(NSString * eventType))succeedBlock failed:(void(^)(NSString * errorMessage))failedBlock;

/* WebView html video 视频曝光
 url:监测的链接
 adView:监测的WKWebView视图对象
 type: 曝光:0；可见曝光：1
 playType: 自动播放:1;手动播放:2;无法识别:0
 */
- (void)webViewVideoImp:(NSString *)url ad:(WKWebView *)adView impressionType:(NSInteger)type  isInjectJs:(BOOL)isInject videoPlayType:(NSInteger)playType succeed:(void(^)(NSString * eventType))succeedBlock failed:(void(^)(NSString * errorMessage))failedBlock;


//停止可见监测
- (void)stop:(NSString *)url;


// 清空待发送任务队列和发送失败任务队列
- (BOOL)clearAll;

// 清空发送失败任务队列
- (BOOL)clearErrorList;

// 进入后台调用
- (void)didEnterBackground;

// 进入前台调用
- (void)didEnterForeground;

// 结束时调用
- (void)willTerminate;


@end
