# 数字广告监测及验证统一SDK (iOS) 部署指南



### 步骤1：添加 iOS SDK 到工程中

1. 将SDK发布文件中，release目录下的**MobileTracking.h** 、**libMobileTracking.a**、**sdkconfig.xml** 三个文件拷贝到项目工程中，将 **sdkconfig.xml** 上传到 web 服务器，使其可以通过 web 方式访问，假设其地址为 **http://xxxxxx.com/sdkconfig.xml**（其后会用到）。
2. 在项目工程 App 的 Target Build Settings 中的 **Other Linker Flags** 选项里添加 **-lxml2** **-all_load** 或 **-lxml2** **-force_load** 静态库的相对路径
3. 添加SDK需要的Framework
在需要添加SDK的项目的 Xcode 开发环境中选择 TARGETS-->Build Phases-->Link Binary With Libraries--> + 添加以下framework框架:

```
    CoreLocation.framework
    libxml2.2.tbd
	AdSupport.framework
	CoreTelephony.framework
 	SystemConfiguration.framework
    WebKit.framework  
```

### 步骤2:使用方法
在使用的文件中引用 
**#import "MobileTracking.h"**.

使用说明:

#### 1、初始化方法
在进行监测之前，必须在主线程进行初始化，通过以下的代码进行初始化操作

```
[MobileTracking sharedInstance]

```

#### 2、配置远程配置文件地址方法

SDK 会自动下载远程的配置文件，使用最新的配置文件进行参数的组装。

```
[[MobileTracking sharedInstance] configFromUrl:@“http://xxxxxx.com/sdkconfig.xml”];
```

#### 3、曝光的监测

通过调用以下的代码进行曝光的监测，

 * xxxImp:参数为第三方公司的监测地址
 * ad:参数为当前广告视图对象（**广告可见曝光监测为必传字段，普通广告监测默认缺省。**）
 * videoPlayType:参数为当前视频广告的播放类型（**视频广告曝光监测为可选字段，1-自动播放，2-手动播放，0-无法识别。**）
 * impressionType:参数为曝光的类型。（**0-普通曝光，1-可见曝光**）
 * succeed:参数为监测成功回调block。（**eventType为对应的曝光类型**）
 * failed:参数为监测失败回调block。 

3.1 display曝光监测

```
 
/* display
url:监测的链接 (必填)
adView:监测的广告视图对象 (必填)
type: 曝光:0；可见曝光：1 (必填)
*/
[[MobileTracking sharedInstance] displayImp:@"http://example.com/xxxxxx” ad:adView impressionType:0 succeed:^(NSString *eventType) {
       //监测代码发送成功
} failed:^(NSString *errorMessage) {
     //监测代码发送失败
}];

```

曝光的定义：只有广告物料已经加载在客户端并至少已经开始渲染（Begin to render，简称BtR）时，才应称之为“曝光”事件。“渲染”指的是绘制物料的过程，或者指将物料添加到文档对象模型的过程。

如果进行曝光调用，则SDK会查验广告素材是否已开始渲染，如果是，则SDK会向监测方发出曝光上报。

备注：对广告进行可见性监测时，广告必须是满足开始渲染（Begin to render，简称BtR）条件的合法曝光，否则SDK不会执行可见监测。在调用可见曝光监测接口时，SDK会查验广告素材是否已开始渲染，如果是，则SDK会向监测方发出曝光上报，并继续进行可见监测，直到满足可见/不可见条件，再结束可见监测流程；如果不是，则SDK不会执行可见监测流程。


3.2 video曝光监测

```
/* video
 url:监测的链接 (必填)
 adView:监测的广告视图对象 (必填)
 type: 曝光:0；可见曝光：1 (必填)
 playType: 自动播放:1;手动播放:2;无法识别:0 (必填)
   
 */
 [[MobileTracking sharedInstance] videoImp:@"http://example.com/xxxxxx” ad:adview impressionType:0 videoPlayType:1 succeed:^(NSString *eventType) {
       //监测代码发送成功
} failed:^(NSString *errorMessage) {
     //监测代码发送失败
}];

```

3.3 WebView 广告素材页面对接

使用说明：
在对WebView实现的广告位执行曝光监测时，ios sdk会向webview动态注入js代码, 主动监测页面广告元素的BtR的状态,为了精确对某个广告素材进行监测,需在该标签元素上增加 `class ='umid-element'`样式标识.

示例代码：

 ```html5
 <!-- 视频广告 -->
 <video width="300" class="umid-element"  controls playsinline muted>
    <source src="./ad.mp4" type="video/mp4">
    <source src="./ad.webm" type="video/webm">
 </video>
<!-- 图片广告 -->
  <div style="margin-bottom: 0px;">
      <img alt="HTML Display Reference Ad" class="umid-element" src="./ads.png">
  </div>
 ```

#### 

3.4 WebView Display曝光监测

使用说明：
当`isInjectJs`为YES时要在WKWebview的didFinishNavigation回调方法中调用，为NO时要在WKWebview的didStartProvisionalNavigation回调方法中调用，否则可能造成SDK无法进行Webview曝光监测。

```
/* WebView html display曝光 
 url:监测的链接 (必填) 
 adView:监测的WKWebView视图对象 (必填)
 type: 曝光:0；可见曝光：1 (必填)
 isInjectJs:是否需要注入 YES/NO,当为NO时，需媒体自行在html内添加相关js代码，详见末尾附件。 (必填)
 */
[[MobileTracking sharedInstance] webViewImp:@"http://example.com/xxxxxx” ad:adview impressionType:1 isInjectJs:YES succeed:^(NSString *eventType) {
       //监测代码发送成功
} failed:^(NSString *errorMessage) {
     //监测代码发送失败
}];

```
3.5 WebView Video曝光监测

使用说明：
当`isInjectJs`为YES时要在WKWebview的didFinishNavigation回调方法中调用，为NO时要在WKWebview的didStartProvisionalNavigation回调方法中调用，否则可能造成SDK无法进行Webview曝光监测。

```
/* WebView html display曝光
 url:监测的链接 (必填) 
 adView:监测的WKWebView视图对象 (必填)
 type: 曝光:0；可见曝光：1 (必填)
 isInjectJs:是否需要注入 YES/NO，当为NO时，需媒体自行在html内添加相关js代码，详见末尾附件。 (必填)
 videoPlayType:广告播放类型 自动播放:1;手动播放:2;无法识别:0
 */
[[MobileTracking sharedInstance] webViewVideoImp:@"http://example.com/xxxxxx” ad:adview impressionType:1 isInjectJs:YES videoPlayType:1 succeed:^(NSString *eventType) {
       //监测代码发送成功
} failed:^(NSString *errorMessage) {
     //监测代码发送失败
}];
```


3.6 可见性广告监测停止，广告播放结束时调用

```
[[MobileTracking sharedInstance] stop:@"http://example.com/xxxxxx”];
```
#### 4、点击监测
通过调用以下的代码进行点击的监测，参数为第三方公司的监测地址

```
[[MobileTracking sharedInstance] click:@"http://example.com/xxxxxx"];
```

#### 5、进入后台时调用
主要用于保存当前监测数据，不被丢失。建议放在AppDelegate的applicationDidEnterBackground方法中

```
[[MobileTracking sharedInstance] didEnterBackground];
```


#### 6、回到前台时调用
重新读取缓存数据，主要用于保证当前监测数据，及时上报,建议放在AppDelegate的applicationWillEnterForeground方法中

```
[[MobileTracking sharedInstance] didEnterForeground];
```


#### 7、应用结束时调用
主要用于保存当前监测数据，不被丢失。

```
[[MobileTracking sharedInstance] willTerminate];
```

#### 8、开启调试日志
建议在测试时候打开

```
[[MobileTracking sharedInstance] enableLog:YES];
```

### 步骤3：验证和调试

SDK 的测试有两个方面：

1. 参数是否齐全，URL 拼接方式是否正确
2. 请求次数和第三方监测平台是否能对应上

请联系第三方监测平台完成测试


 ### 附件：

 #### WebView 广告素材页面对接 (方案二)
 使用说明: 采用不注入的方式,需要事先埋入 js sdk 代码片段。 sdk 默认会主动监测页面广告元素的 BTR 的状态, 如果您想精确对某个广告素材进行监测, 只需加上在该标签元素上增加 `class ='umid-element'`样式标识。

示例代码:

```html5
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <!-- js sdk 代码放入 head 元素内 -->
    <script>
    !function(){"use strict";var e=function(e){return JSON.stringify(e)},t=function(e){var t=e.getBoundingClientRect();return 0==t.width||0==t.height};var n,i,s=function(){return(s=Object.assign||function(e){for(var t,n=1,i=arguments.length;n<i;n++)for(var s in t=arguments[n])Object.prototype.hasOwnProperty.call(t,s)&&(e[s]=t[s]);return e}).apply(this,arguments)},o=[],r=new window.Map,a=function(e){var t;navigator.userAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/)?window.webkit.messageHandlers.__mz_Monitor.postMessage(e):((t=navigator.userAgent).indexOf("Android")>-1||t.indexOf("Adr")>-1)&&window.__mz_Monitor.mz_push(e)},d=function(t){var n=e(t);r.has(n)||(r.set(n),o.push(e(s(s({},t),{time:(new Date).toISOString()})))),function(){for(;o.length;){var e=o.shift();e&&a(e)}}()};!function(e){e.JavaScript="javaScript",e.Image="image",e.Video="video",e.AUDIO="audio",e.HTML="html"}(n||(n={})),function(e){e.Impression="impression",e.ImpressionError="impressionError",e.WindowUnload="unload",e.Click="click",e.DisplayChange="displayChange",e.Start="start",e.FirstQuartile="firstQuartile",e.Midpoint="midpoint",e.ThirdQuartile="thirdQuartile",e.Complete="complete",e.Pause="pause",e.Resume="resume",e.BufferStart="bufferStart",e.BufferFinish="bufferFinish"}(i||(i={}));var h=function(){function e(e){this.advertisementType=n.Image,this.el=e,this.onError=this.onError.bind(this),this.onLoad=this.onLoad.bind(this),this.bindEvent(),this.checkVisible()}return e.prototype.checkVisible=function(){this.el.complete&&(t(this.el)?this.onError():this.imageComplete())},e.prototype.imageComplete=function(){var e=new Image;e.src=this.el.src,e.onload=this.onLoad,e.onerror=this.onError},e.prototype.bindEvent=function(){this.el.addEventListener("error",this.onError),this.el.addEventListener("load",this.onLoad)},e.prototype.send=function(e){d(s(s({},e),{advertisementType:this.advertisementType}))},e.prototype.onImpression=function(){this.send({eventType:i.Impression,ImpressionType:"beginToRender",isRender:"1"})},e.prototype.onError=function(){this.send({eventType:i.ImpressionError,ImpressionType:"beginToRender",isRender:"0"})},e.prototype.onLoad=function(){this.onImpression()},e.prototype.onClick=function(){},e.prototype.destroyed=function(){this.el.removeEventListener("error",this.onError),this.el.removeEventListener("load",this.onLoad),this.el=null},e}(),u=function(){function e(e){this.advertisementType=n.AUDIO,this.quartileEvents={"1q":!1,"2q":!1,"3q":!1,"4q":!1},this.isFirst=!1,this.isFullscreen=!1,this.el=e,this.isFirst=!0,this.onPlay=this.onPlay.bind(this),this.onPause=this.onPause.bind(this),this.onWaiting=this.onWaiting.bind(this),this.onTimeupdate=this.onTimeupdate.bind(this),this.onWebkitendfullscreen=this.onWebkitendfullscreen.bind(this),this.onWebkitbeginfullscreen=this.onWebkitbeginfullscreen.bind(this),this.bindEvent(),this.checkElement()}return e.prototype.checkElement=function(){t(this.el)?this.beforeImpression(!1):this.beforeImpression(!0)},e.prototype.bindEvent=function(){this.el.addEventListener("play",this.onPlay),this.el.addEventListener("pause",this.onPause),this.el.addEventListener("waiting",this.onWaiting),this.el.addEventListener("timeupdate",this.onTimeupdate),this.el.addEventListener("webkitendfullscreen",this.onWebkitendfullscreen),this.el.addEventListener("webkitbeginfullscreen",this.onWebkitbeginfullscreen)},e.prototype.onPlay=function(){this.isFirst?this.isFirst=!1:this.send({EventType:i.Resume})},e.prototype.onPause=function(){this.send({eventType:i.Pause})},e.prototype.onWaiting=function(){},e.prototype.onTimeupdate=function(){},e.prototype.onVolumechange=function(){},e.prototype.onWebkitendfullscreen=function(){this.isFullscreen=!1},e.prototype.onWebkitbeginfullscreen=function(){this.isFullscreen=!0},e.prototype.send=function(e){d(s(s({},e),{advertisementType:this.advertisementType,quartileEvents:this.quartileEvents}))},e.prototype.beforeImpression=function(e){this.send({EventType:i.Impression,ImpressionType:"beginToRender",isRender:e?"1":"0"})},e.prototype.destroyed=function(){this.el.removeEventListener("play",this.onPlay),this.el.removeEventListener("pause",this.onPause),this.el.removeEventListener("waiting",this.onWaiting),this.el.removeEventListener("timeupdate",this.onTimeupdate),this.el.removeEventListener("webkitendfullscreen",this.onWebkitendfullscreen),this.el.removeEventListener("webkitbeginfullscreen",this.onWebkitbeginfullscreen),this.el=null},e}(),l=function(){function e(){this.advertisementType=n.HTML,document.body.innerText.length>0?this.send({isRender:"1"}):this.send({isRender:"0"})}return e.prototype.send=function(e){d(s(s({},e),{advertisementType:this.advertisementType,ImpressionType:"beginToRender"}))},e}();var p,c,f=function(){var e,t=document.body.querySelector(".umid-element");if(t)return(e=t).tagName&&"video"===e.tagName.toLowerCase()?new u(t):function(e){return e.tagName&&"img"===e.tagName.toLowerCase()}(t)?new h(t):new l;var n=document.body.querySelector("video");if(n)return new u(n);var i=document.body.querySelectorAll("img");if(i.length){var s=function(e){for(var t=0,n=null,i=0;i<e.length;i++){var s=e[i],o=s.offsetWidth*s.offsetHeight;o>=t&&(t=o,n=s)}return n}(i);return new h(s)}return new l};function m(){f()}if(document.body)m();else{var v=(p=function(){m()},c=!1,function(){c||(c=!0,p.apply(this,arguments))});window.addEventListener("load",v),document.addEventListener("DOMContentLoaded",v)}}();
    </script>
  </head>
  <body>
    <video width="300" class="omid-element" controls playsinline  muted>
      <source src="./ad.mp4" type="video/mp4">
      <source src="./ad.webm" type="video/webm">
    </video>
</body>
</html>

```
